# <!-- coding: utf-8 -->
#
# quelques fonctions pour la localisation des carres
# auteur: Marc Gauthier
#
#
# source("geo/scripts/saintlu.R"); geocode_jour()
geocode_jour <- function() {
  carp()
  geocode_carres();
}

geocode_carres <- function() {
  carp()
  carres.sf <- couches_grille_lire()
  nc <- geocode_reverse_sf(carres.sf);
}