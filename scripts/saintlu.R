# <!-- coding: utf-8 -->
#
# quelques fonctions pour saintlu (demande de Régis )
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
mga  <- function() {
  source("geo/scripts/saintlu.R");
}
#
# les variables globales
Drive <- substr( getwd(),1,2)
baseDir <- sprintf("%s/web", Drive)
cfgDir <- sprintf("%s/web/geo/%s", Drive, "SAINTLU")
varDir <- sprintf("%s/bvi35/CouchesSaintLu", Drive);
texDir <- sprintf("%s/web/geo/%s", Drive, "SAINTLU")
dir.create(varDir, showWarnings = FALSE, recursive = TRUE)
dir.create(texDir, showWarnings = FALSE, recursive = TRUE)
ignDir <- sprintf("%s/bvi35/CouchesIGN", Drive);
webDir <- sprintf("%s/web.heb/bv/saintlu", Drive);
#
# les bibliothèques
setwd(baseDir)
source("geo/scripts/mga.R");
source("geo/scripts/misc.R");
source("geo/scripts/misc_couches.R");
source("geo/scripts/misc_fonds.R");
source("geo/scripts/misc_gdal.R");
source("geo/scripts/misc_geocode.R");
source("geo/scripts/misc_ocs.R");
source("geo/scripts/misc_tex.R");
source("geo/scripts/saintlu_cartes.R");
source("geo/scripts/saintlu_cbnb.R");
source("geo/scripts/saintlu_cesbio.R");
source("geo/scripts/saintlu_couches.R");
source("geo/scripts/saintlu_donnees.R");
source("geo/scripts/saintlu_geocode.R");
source("geo/scripts/saintlu_ocs.R");
source("geo/scripts/saintlu_rpg.R");
source("geo/scripts/saintlu_suivi.R");
#
# les commandes permettant le lancement
DEBUG <- FALSE
if ( interactive() ) {
  carp("interactive")
# un peu de nettoyage
  graphics.off()
  par(mar=c(0,0,0,0), oma=c(0,0,0,0))
} else {
  carp("console")
}
